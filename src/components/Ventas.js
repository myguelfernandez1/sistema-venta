import React, { Component } from "react";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Text, View, ScrollView, TouchableOpacity, StyleSheet, StatusBar, TextInput } from 'react-native';
import { Table, TableWrapper, Row, Rows, Col, Cell } from 'react-native-table-component'
import AppButton from "./AppButton";
import { Dropdown } from 'react-native-element-dropdown';
import AntDesign from '@expo/vector-icons/AntDesign';
import { printToFileAsync } from 'expo-print';
import { shareAsync } from 'expo-sharing';


const stylesTable = StyleSheet.create({
    container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
    head: { height: 40, backgroundColor: '#808B97' },
    text: { margin: 6 },
    row: { flexDirection: 'row', backgroundColor: '#E7E6E1' },
    btn: { width: 58, height: 18, backgroundColor: '#78B7BB', borderRadius: 2 },
    btnText: { textAlign: 'center', color: '#fff' }
});
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        backgroundColor: '#E0E0E0',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
    },
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
    },
    dropdown: {
        height: 50,
        borderColor: 'gray',
        borderWidth: 0.5,
        borderRadius: 8,
        paddingHorizontal: 8,
    },
    placeholderStyle: {
        fontSize: 16,
    },
    selectedTextStyle: {
        fontSize: 16,
    },
    iconStyle: {
        width: 20,
        height: 20,
    },
    inputSearchStyle: {
        height: 40,
        fontSize: 16,
    }
});

export default class Ventas extends Component {


    constructor() {
        super();
        this.state = {
            ventas: [],
            tableHead: ['Id', 'Factura', 'Fecha', 'Total', ''],
            tableHeadDetalle: ['Producto', 'Precio', 'Cantidad', ''],
            pantalla: 'lista',
            venta: {
                detalles: [],
                total: 0
            },
            detalle: {},
            productos: [],
            categorias: [],
            detalles2: []
        }
        AsyncStorage.setItem('recargarVentas', "true");

    }


    async getItem(item) {
        const result = await AsyncStorage.getItem(item);
        return JSON.parse(result);
    }

    async addVenta() {
        console.log('add venta');
        this.setState({
            pantalla: 'nuevo',
            venta: {
                id: this.getMaxIdPlus(),
                factura: '001-001-00000' + this.getMaxIdPlus(),
                fecha: this.date_TO_String(),
                total: 0,
                detalles: [],
            },
            detalle: {},
            detalles2: []
        })
    }

    async calcularVenta() {
        let totalTemp = 0;
        this.state.venta.detalles.map(
            item => {
                totalTemp = totalTemp + (item.precio * item.cantidad)
            }
        )
        this.state.venta.total = totalTemp;
        this.setState({
            venta: this.state.venta,
            pantalla: 'nuevo'
        })
    }


    cancelar() {
        this.setState({
            pantalla: "lista"
        })
    }

    async addDetalle() {
        console.log(this.state);
        AsyncStorage.setItem('detalles', JSON.stringify(this.state.venta.detalles));
        this.getItem('productos').then(
            productos => {
                console.log(productos);
                let productosTemp = [];
                productos.map(
                    producto => {
                        console.log(producto);
                        const p = {
                            label: producto.nombre + '/' + producto.precio,
                            value: producto.nombre + '/' + producto.precio
                        }
                        productosTemp.push(p);
                    }
                );
                this.setState({
                    productos: productosTemp,
                    pantalla: 'addDetalle'
                })

            }
        )
    }

    async guardarVenta() {
        console.log('guardar venta');
        console.log(this.state.venta);
        this.state.ventas.push(this.state.venta);
        console.log(this.state);
        AsyncStorage.setItem('ventas', JSON.stringify(this.state.ventas));
        this.setState({
            pantalla: 'lista',
            ventas: this.state.ventas
        });
    }

    async detalles() {
        console.log(this.state.venta);
        this.setState({
            pantalla: "detalles"
        });
    }

    async addDetalles() {
        console.log(this.state.detalle);
        console.log(this.state.venta.detalles);
        console.log(this.state.detalles2);
        this.state.venta.detalles.push(this.state.detalle);

        this.getItem('detalles').then(
            data => {
                console.log(data);
                if (data) {
                    data.push(this.state.detalle);
                    this.state.venta.detalles = data;
                    this.setState({
                        pantalla: "detalles",
                        venta: this.state.venta
                    });
                } else {
                    this.setState({
                        pantalla: "detalles",
                        venta: this.state.venta
                    });
                }

            }
        );


    }


    removeDetalle(id) {
        console.log(id);
        let eliminar = {};
        this.state.venta.detalles.map(data => {
            if (data.id == id) {
                eliminar = data;
            }
        })
        const index = this.state.venta.detalles.indexOf(eliminar);
        this.state.venta.detalles.splice(index, 1);
        this.setState({ veta: this.state.venta });
        // AsyncStorage.setItem('ventas', JSON.stringify(this.state.ventas));

    }

    verDetalle(id) {
        let ventaVer = {};
        this.state.ventas.map(data => {
            if (data.id == id) {
                ventaVer = data;
            }
        })
        this.setState({
            venta: ventaVer,
            pantalla: 'verDetalles'
        })
    }



    removeVenta(id) {
        console.log(id);
        let eliminar = {};
        this.state.ventas.map(data => {
            if (data.id == id) {
                eliminar = data;
            }
        })
        const index = this.state.ventas.indexOf(eliminar);
        this.state.ventas.splice(index, 1);
        this.setState({ ventas: this.state.ventas });
        AsyncStorage.setItem('ventas', JSON.stringify(this.state.ventas));

    }

    getMaxIdPlus() {
        let id = 0;
        this.state.ventas.map(data => {
            if (data.id > id) {
                id = data.id
            }
        })
        return id + 1;
    }

    date_TO_String() {
        let date_Object = new Date();
        var date_String = date_Object.getFullYear() +
            "/" +
            (date_Object.getMonth() + 1) +
            "/" +
            +date_Object.getDate() +
            " " +
            +date_Object.getHours() +
            ":" +
            +date_Object.getMinutes();
        return date_String;
    }


    getRowData(lista) {
        const tableData = [];
        lista.map(data => {
            const rowData = [];
            rowData.push(`${data.id}`);
            rowData.push(`${data.factura}`);
            rowData.push(`${data.fecha}`);
            rowData.push(`${data.total}`);
            rowData.push('editar');
            tableData.push(rowData);
        })
        console.log(tableData);
        return tableData;
    }
    getRowDataDetalle(lista) {
        console.log(this.state.venta.detalles);
        const tableData = [];
        this.state.venta.detalles.map(data => {
            const rowData = [];
            rowData.push(`${data.producto}`);
            rowData.push(`${data.precio}`);
            rowData.push(`${data.cantidad}`);
            rowData.push('editar');
            tableData.push(rowData);
        })
        console.log(tableData);
        return tableData;
    }

    getRowDataVerDetalle(lista) {
        console.log(this.state.venta.detalles);
        const tableData = [];
        this.state.venta.detalles.map(data => {
            const rowData = [];
            rowData.push(`${data.producto}`);
            rowData.push(`${data.precio}`);
            rowData.push(`${data.cantidad}`);
            // rowData.push('editar');
            tableData.push(rowData);
        })
        console.log(tableData);
        return tableData;
    }




    generatePdf = async (id) => {
        let ventaVer = {};
        this.state.ventas.map(data => {
            if (data.id == id) {
                ventaVer = data;
            }
        })
        console.log(ventaVer);
        const html = `
            <html>
            <body>
                <h1> Fecha ${ventaVer.fecha}</h1>
            </body>
            </html>
        `;
        const file = await printToFileAsync({
            html: html,
            base64: false
        }).then(
            data => {
                console.log("hola");
                console.log(file);
            }
        );
        console.log(file);

        await shareAsync(file.uri);
    };

    render() {
        let state = this.state;
        let generatePdf = async () => {
            const file = await printToFileAsync({
                html: html,
                base64: false
            });

            await shareAsync(file.uri);
        };
        this.getItem("recargarVentas").then(
            recargar => {
                console.log(recargar);
                if (recargar) {
                    this.getItem("ventas").then(
                        data => {
                            console.log(data);
                            if (data) {
                                console.log(data);
                                this.setState({
                                    ventas: data,
                                    pantalla: "lista"
                                });
                            } else {
                                const ventasTemp = [{
                                    id: 1,
                                    factura: '001-001-000123',
                                    fecha: '2023-11-20',
                                    total: 10000
                                }]
                                AsyncStorage.setItem('ventas', JSON.stringify(ventasTemp));

                            }

                        }
                    );
                    AsyncStorage.setItem('recargarVentas', "false");
                }

            }

        );

        console.log(this.state.venta);
        const tableDataVentas = this.getRowData(this.state.ventas);
        const tableDataDetalle = this.getRowDataDetalle(this.state.venta.detalles);
        const element = (id, data, index) => (
            <View>
                <TouchableOpacity onPress={() => this.verDetalle(id)}>
                    <View style={{ width: 58, height: 18, backgroundColor: 'blue', borderRadius: 2 }}>
                        <Text style={stylesTable.btnText}>Detalle</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.generatePdf(id)}>
                    <View style={{ width: 58, height: 18, backgroundColor: 'green', borderRadius: 2 }}>
                        <Text style={stylesTable.btnText}>PDF</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.removeVenta(id)}>
                    <View style={{ width: 58, height: 18, backgroundColor: 'red', borderRadius: 2 }}>
                        <Text style={stylesTable.btnText}>Eliminar</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
        const elementDetalle = (id, data, index) => (
            <View>
                <TouchableOpacity onPress={() => this.removeDetalle(id)}>
                    <View style={{ width: 58, height: 18, backgroundColor: 'red', borderRadius: 2 }}>
                        <Text style={stylesTable.btnText}>Eliminar</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
        if (state.pantalla == 'lista') {
            return (
                <View>
                    <View style={{ flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' }}>
                        <ScrollView horizontal={true}>
                            <Table borderStyle={{ borderWidth: 1, borderColor: '#C1C0B9' }}>
                                <Row data={state.tableHead} style={stylesTable.head} textStyle={stylesTable.text} />
                                {
                                    tableDataVentas.map((rowData, index) => (
                                        <TableWrapper key={index} style={stylesTable.row}>
                                            {
                                                rowData.map((cellData, cellIndex) => (
                                                    <Cell key={cellIndex} data={cellIndex === 4 ? element(rowData[0], cellData, index) : cellData} textStyle={stylesTable.text} />
                                                ))
                                            }
                                        </TableWrapper>
                                    ))
                                }
                            </Table>

                        </ScrollView>
                        <AppButton
                            bgColor="rgba(14, 213, 100, 0.57)"
                            title="Agrega Venta"
                            action={this.addVenta.bind(this)}
                            iconName="plus"
                            iconSize={30}
                            iconColor="#fff"
                        />
                    </View>
                </View>
            );
        } else if (state.pantalla == 'detalles') {
            console.log(this.state);
            try {
                let totalTemp = 0;
                this.state.venta.detalles.map(
                    data => {
                        totalTemp = totalTemp + (data.precio * data.cantidad);
                    }
                );
                console.log(totalTemp);
                this.state.venta.total = totalTemp;
                // this.setState({
                //     venta:  this.state.venta
                // });
            } catch (error) {

            }
            return (
                <ScrollView>
                    <Text>Total Detalles: {this.state.venta.total} Gs.</Text>
                    <View>
                        <View style={{ flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' }}>
                            <ScrollView horizontal={true}>
                                <Table borderStyle={{ borderWidth: 1, borderColor: '#C1C0B9' }}>
                                    <Row data={state.tableHeadDetalle} style={stylesTable.head} textStyle={stylesTable.text} />
                                    {
                                        tableDataDetalle.map((rowData, index) => (
                                            <TableWrapper key={index} style={stylesTable.row}>
                                                {
                                                    rowData.map((cellData, cellIndex) => (
                                                        <Cell key={cellIndex} data={cellIndex === 3 ? element(rowData[0], cellData, index) : cellData} textStyle={stylesTable.text} />
                                                    ))
                                                }
                                            </TableWrapper>
                                        ))
                                    }
                                </Table>

                            </ScrollView>
                            <AppButton
                                bgColor="rgba(14, 213, 100, 0.57)"
                                title="Agregar Detalle"
                                action={this.addDetalle.bind(this)}
                                iconName="plus"
                                iconSize={30}
                                iconColor="#fff"
                            />
                            <AppButton
                                bgColor="rgba(14, 213, 100, 0.57)"
                                title="Guardar"
                                action={this.calcularVenta.bind(this)}
                                iconName="plus"
                                iconSize={30}
                                iconColor="#fff"
                            />
                        </View>
                    </View>
                </ScrollView>
            );
        } else if (state.pantalla == 'verDetalles') {
            console.log(this.state);
            const tableDataVerDetalle = this.getRowDataVerDetalle(this.state.venta.detalles);
            try {
                let totalTemp = 0;
                this.state.venta.detalles.map(
                    data => {
                        totalTemp = totalTemp + (data.precio * data.cantidad);
                    }
                );
                console.log(totalTemp);
                this.state.venta.total = totalTemp;
                // this.setState({
                //     venta:  this.state.venta
                // });
            } catch (error) {

            }
            return (
                <ScrollView>
                    <Text>Total Detalles: {this.state.venta.total} Gs.</Text>
                    <View>
                        <View style={{ flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' }}>
                            <ScrollView horizontal={true}>
                                <Table borderStyle={{ borderWidth: 1, borderColor: '#C1C0B9' }}>
                                    <Row data={state.tableHeadVerDetalle} style={stylesTable.head} textStyle={stylesTable.text} />
                                    {
                                        tableDataVerDetalle.map((rowData, index) => (
                                            <TableWrapper key={index} style={stylesTable.row}>
                                                {
                                                    rowData.map((cellData, cellIndex) => (
                                                        <Cell key={cellIndex} data={cellIndex === 3 ? element(rowData[0], cellData, index) : cellData} textStyle={stylesTable.text} />
                                                    ))
                                                }
                                            </TableWrapper>
                                        ))
                                    }
                                </Table>

                            </ScrollView>
                            <AppButton
                                bgColor="rgba(14, 213, 100, 0.57)"
                                title="PDF"
                                action={this.generatePdf.bind(this)}
                            // iconName="plus"
                            // iconSize={30}
                            // iconColor="#fff"
                            />

                            <AppButton
                                bgColor="rgba(14, 213, 100, 0.57)"
                                title="Aceptar"
                                action={this.cancelar.bind(this)}
                            // iconName="plus"
                            // iconSize={30}
                            // iconColor="#fff"
                            />
                        </View>
                    </View>
                </ScrollView >
            );
        } else if (this.state.pantalla == "addDetalle") {
            let value = null;
            let detalle = this.state.detalle;
            console.log(this.state.venta.detalles);
            this.state.detalles2 = JSON.parse(JSON.stringify(this.state.venta.detalles))
            console.log(this.state.detalles2);
            // this.state.detalle = {}
            return (

                <ScrollView>
                    <Dropdown
                        style={styles.dropdown}
                        placeholderStyle={styles.placeholderStyle}
                        selectedTextStyle={styles.selectedTextStyle}
                        inputSearchStyle={styles.inputSearchStyle}
                        iconStyle={styles.iconStyle}
                        data={this.state.productos}
                        search
                        maxHeight={300}
                        labelField="label"
                        valueField="value"
                        placeholder="Producto"
                        value={value}
                        onChange={item => {
                            console.log(item.value);
                            detalle.producto = item.value.split('/')[0];
                            detalle.precio = item.value.split('/')[1];
                            console.log(detalle);
                            console.log(this.state);
                            this.setState({ detalle })
                        }}
                        renderLeftIcon={() => (
                            <AntDesign style={styles.icon} color="black" name="Safety" size={20} />
                        )}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder='Cantidad'
                        value={detalle.cantidad}
                        onChangeText={(value) => {
                            detalle.cantidad = value.replace(/[^0-9]/g, '');
                            this.setState({ detalle })
                        }}
                    />
                    <AppButton
                        bgColor="rgba(14, 213, 100, 0.57)"
                        title="Agregar"
                        action={this.addDetalles.bind(this)}
                        iconName="plus"
                        iconSize={30}
                        iconColor="#fff"
                    />
                </ScrollView>
            );
        } else {
            let venta = this.state.venta;
            let value = null;
            return (
                <ScrollView>
                    <TextInput
                        style={styles.input}
                        placeholder='ID'
                        editable={false}
                        value={venta.id.toString()}
                        onChangeText={(value) => {
                            venta.id = value;
                            this.setState({ venta: venta })
                            // console.log(venta);
                        }}
                    />

                    <TextInput
                        style={styles.input}
                        placeholder='Factura'
                        value={venta.factura}
                        editable={false}
                        onChangeText={(value) => {
                            venta.factura = value;
                            this.setState({ venta: venta })
                        }}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder='Fecha'
                        value={venta.fecha}
                        editable={false}
                        onChangeText={(value) => {
                            venta.fecha = value;
                            this.setState({ venta: venta })
                        }}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder='Total'
                        value={venta.total}
                        editable={false}
                        onChangeText={(value) => {
                            venta.total = value;
                            this.setState({ venta: venta })
                        }}
                    />

                    <AppButton
                        bgColor="rgba(14, 213, 100, 0.57)"
                        title="Detalles"
                        action={this.detalles.bind(this)}
                        iconName="plus"
                        iconSize={30}
                        iconColor="#fff"
                    />
                    <AppButton
                        bgColor="rgba(14, 213, 100, 0.57)"
                        title="Registrar Venta"
                        action={this.guardarVenta.bind(this)}
                        iconName="plus"
                        iconSize={30}
                        iconColor="#fff"
                    />
                    <AppButton
                        bgColor="rgba(14, 213, 100, 0.57)"
                        title="Cancelar"
                        action={this.cancelar.bind(this)}
                    // iconName="plus"
                    // iconSize={30}
                    // iconColor="#fff"
                    />
                </ScrollView>
            );

        }

    }
}