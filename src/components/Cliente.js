import React, { Component } from "react";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Text, View, ScrollView, TouchableOpacity, StyleSheet, StatusBar, TextInput } from 'react-native';
import { Table, TableWrapper, Row, Rows, Col, Cell } from 'react-native-table-component'
import AppButton from "./AppButton";


const stylesTable = StyleSheet.create({
    container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
    head: { height: 40, backgroundColor: '#808B97' },
    text: { margin: 6 },
    row: { flexDirection: 'row', backgroundColor: '#E7E6E1' },
    btn: { width: 58, height: 18, backgroundColor: '#78B7BB', borderRadius: 2 },
    btnText: { textAlign: 'center', color: '#fff' }
});
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        backgroundColor: '#E0E0E0',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
    },
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
    }
});

export default class Clientes extends Component {


    constructor() {
        super();
        this.state = {
            clientes: [],
            tableHead: ['Id', 'Nombre', 'Apellido', 'Ruc', 'Email', ''],
            pantalla: 'lista',
            cliente: {}
        }
        AsyncStorage.setItem('recargarClientes', "true");

    }


    async getItem(item) {
        const result = await AsyncStorage.getItem(item);
        return JSON.parse(result);
    }

    async addCliente() {
        console.log('add cliente');
        this.setState({
            pantalla: 'nuevo',
            cliente: {
                id: this.getMaxIdPlus(),
                nombre: ''
            }
        });
    }

    async guardarCliente() {
        console.log('guardar cliente');
        console.log(this.state.cliente);
        this.state.clientes.push(this.state.cliente);
        console.log(this.state);
        AsyncStorage.setItem('clientes', JSON.stringify(this.state.clientes));
        this.setState({
            pantalla: 'lista',
            clientes: this.state.clientes
        });
    }


    editCliente(id) {
        console.log(id);
        let editar = {};
        this.state.clientes.map(data => {
            if (data.id == id) {
                editar = data;
            }
        })
        console.log(editar);
        this.setState({
            cliente: editar,
            pantalla: "editar"
        })

    }

    confirmarEditarCliente() {
        console.log('confirmar edicion');
        console.log(this.state.cliente);
        let index = -1;
        this.state.clientes.map(
            item => {
                if (item.id == this.state.cliente.id) {
                    index = this.state.clientes.indexOf(item);
                }
            }
        )
        this.state.clientes[index] = this.state.cliente;
        AsyncStorage.setItem('clientes', JSON.stringify(this.state.clientes));
        this.setState({
            clientes: this.state.clientes,
            pantalla: "lista"
        });
    }

    removeCliente(id) {
        console.log(id);
        let eliminar = {};
        this.state.clientes.map(data => {
            if (data.id == id) {
                eliminar = data;
            }
        })
        const index = this.state.clientes.indexOf(eliminar);
        this.state.clientes.splice(index, 1);
        this.setState({ clientes: this.state.clientes });
        AsyncStorage.setItem('clientes', JSON.stringify(this.state.clientes));

    }

    getMaxIdPlus() {
        let id = 0;
        this.state.clientes.map(data => {
            if (data.id > id) {
                id = data.id
            }
        })
        return id + 1;
    }


    getRowData(lista) {
        const tableData = [];
        lista.map(data => {
            const rowData = [];
            rowData.push(`${data.id}`);
            rowData.push(`${data.nombre}`);
            rowData.push(`${data.apellido}`);
            rowData.push(`${data.ruc}`);
            rowData.push(`${data.email}`);
            rowData.push('editar');
            tableData.push(rowData);
        })
        console.log(tableData);
        return tableData;
    }

    render() {
        let state = this.state;
        this.getItem("recargarClientes").then(
            recargar => {
                console.log(recargar);
                if (recargar) {
                    this.getItem("clientes").then(
                        data => {
                            console.log(data);
                            if (data) {
                                console.log(data);
                                this.setState({
                                    clientes: data
                                });
                            } else {
                                const clientesTemp = [{
                                    id: 1,
                                    nombre: 'Miguel',
                                    apellido: 'Fernandez',
                                    ruc: '3810131-9',
                                    email: 'miguel.fernandez@gmail.com'
                                }]
                                AsyncStorage.setItem('clientes', JSON.stringify(clientesTemp));

                            }

                        }
                    );
                    AsyncStorage.setItem('recargarClientes', "false");
                }

            }

        );

        const tableDataClientes = this.getRowData(this.state.clientes);
        const element = (id, data, index) => (
            <View>
                <TouchableOpacity onPress={() => this.editCliente(id)}>
                    <View style={stylesTable.btn}>
                        <Text style={stylesTable.btnText}>Editar</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.removeCliente(id)}>
                    <View style={{ width: 58, height: 18, backgroundColor: 'red', borderRadius: 2 }}>
                        <Text style={stylesTable.btnText}>Eliminar</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
        if (state.pantalla == 'lista') {
            return (
                <View>
                    <View style={{ flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' }}>
                        <ScrollView horizontal={true}>
                            <Table borderStyle={{ borderWidth: 1, borderColor: '#C1C0B9' }}>
                                <Row data={state.tableHead} style={stylesTable.head} textStyle={stylesTable.text} />
                                {
                                    tableDataClientes.map((rowData, index) => (
                                        <TableWrapper key={index} style={stylesTable.row}>
                                            {
                                                rowData.map((cellData, cellIndex) => (
                                                    <Cell key={cellIndex} data={cellIndex === 5 ? element(rowData[0], cellData, index) : cellData} textStyle={stylesTable.text} />
                                                ))
                                            }
                                        </TableWrapper>
                                    ))
                                }
                            </Table>

                        </ScrollView>
                        <AppButton
                            bgColor="rgba(14, 213, 100, 0.57)"
                            title="Agrega Cliente"
                            action={this.addCliente.bind(this)}
                            iconName="plus"
                            iconSize={30}
                            iconColor="#fff"
                        />
                    </View>
                </View>
            );
        } else if (state.pantalla == 'nuevo') {
            let cliente = this.state.cliente;
            return (
                <ScrollView>
                    <TextInput
                        style={styles.input}
                        placeholder='ID'
                        editable={false}
                        value={cliente.id.toString()}
                        onChangeText={(value) => {
                            cliente.id = value;
                            this.setState({ cliente: cliente })
                            // console.log(cliente);
                        }}
                    />

                    <TextInput
                        style={styles.input}
                        placeholder='Nombre'
                        value={cliente.nombre}
                        onChangeText={(value) => {
                            cliente.nombre = value;
                            this.setState({ cliente: cliente })
                        }}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder='Apellido'
                        value={cliente.apellido}
                        onChangeText={(value) => {
                            cliente.apellido = value;
                            this.setState({ cliente: cliente })
                        }}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder='Ruc'
                        value={cliente.ruc}
                        onChangeText={(value) => {
                            cliente.ruc = value;
                            this.setState({ cliente: cliente })
                        }}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder='Email'
                        value={cliente.email}
                        onChangeText={(value) => {
                            cliente.email = value;
                            this.setState({ cliente: cliente })
                        }}
                    />
                    <AppButton
                        bgColor="rgba(14, 213, 100, 0.57)"
                        title="Guardar Cliente"
                        action={this.guardarCliente.bind(this)}
                        iconName="plus"
                        iconSize={30}
                        iconColor="#fff"
                    />
                </ScrollView>
            );

        } else {
            let cliente = this.state.cliente;
            return (
                <ScrollView>
                    <TextInput
                        style={styles.input}
                        placeholder='ID'
                        editable={false}
                        value={cliente.id.toString()}
                        onChangeText={(value) => {
                            cliente.id = value;
                            this.setState({ cliente: cliente })
                            // console.log(cliente);
                        }}
                    />

                    <TextInput
                        style={styles.input}
                        placeholder='Nombre'
                        value={cliente.nombre}
                        onChangeText={(value) => {
                            cliente.nombre = value;
                            this.setState({ cliente: cliente })
                        }}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder='Apellido'
                        value={cliente.apellido}
                        onChangeText={(value) => {
                            cliente.apellido = value;
                            this.setState({ cliente: cliente })
                        }}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder='Ruc'
                        value={cliente.ruc}
                        onChangeText={(value) => {
                            cliente.ruc = value;
                            this.setState({ cliente: cliente })
                        }}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder='Email'
                        value={cliente.email}
                        onChangeText={(value) => {
                            cliente.email = value;
                            this.setState({ cliente: cliente })
                        }}
                    />

                    <AppButton
                        bgColor="rgba(14, 213, 100, 0.57)"
                        title="Editar Cliente"
                        action={this.confirmarEditarCliente.bind(this)}
                        iconName="plus"
                        iconSize={30}
                        iconColor="#fff"
                    />
                </ScrollView>
            );

        }

    }
}