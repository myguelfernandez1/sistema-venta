import React, { Component } from "react";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Text, View, ScrollView, TouchableOpacity, StyleSheet, StatusBar, TextInput } from 'react-native';
import { Table, TableWrapper, Row, Rows, Col, Cell } from 'react-native-table-component'
import AppButton from "./AppButton";
import { Dropdown } from 'react-native-element-dropdown';
import AntDesign from '@expo/vector-icons/AntDesign';


const stylesTable = StyleSheet.create({
    container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
    head: { height: 40, backgroundColor: '#808B97' },
    text: { margin: 6 },
    row: { flexDirection: 'row', backgroundColor: '#E7E6E1' },
    btn: { width: 58, height: 18, backgroundColor: '#78B7BB', borderRadius: 2 },
    btnText: { textAlign: 'center', color: '#fff' }
});
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        backgroundColor: '#E0E0E0',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
    },
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
    },
    dropdown: {
        height: 50,
        borderColor: 'gray',
        borderWidth: 0.5,
        borderRadius: 8,
        paddingHorizontal: 8,
    },
    placeholderStyle: {
        fontSize: 16,
    },
    selectedTextStyle: {
        fontSize: 16,
    },
    iconStyle: {
        width: 20,
        height: 20,
    },
    inputSearchStyle: {
        height: 40,
        fontSize: 16,
    }
});

export default class Productos extends Component {


    constructor() {
        super();
        this.state = {
            productos: [],
            tableHead: ['Id', 'Nombre', 'codigo', 'Categoria', 'Precio', ''],
            pantalla: 'lista',
            producto: {},
            categorias: []
        }
        AsyncStorage.setItem('recargarProductos', "true");

    }


    async getItem(item) {
        const result = await AsyncStorage.getItem(item);
        return JSON.parse(result);
    }

    async addProducto() {
        console.log('add producto');
        this.getItem('categorias').then(
            categorias => {
                console.log(categorias);
                let categoriasTemp = [];
                categorias.map(
                    categoria => {
                        console.log(categoria);
                        const c = {
                            label: categoria.nombre,
                            value: categoria.nombre
                        }
                        categoriasTemp.push(c);
                    }
                );
                this.setState({
                    categorias: categoriasTemp,
                    pantalla: 'nuevo',
                    producto: {
                        id: this.getMaxIdPlus(),
                        nombre: ''
                    }
                })

            }
        )
    }

    async guardarProducto() {
        console.log('guardar producto');
        console.log(this.state.producto);
        this.state.productos.push(this.state.producto);
        console.log(this.state);
        AsyncStorage.setItem('productos', JSON.stringify(this.state.productos));
        this.setState({
            pantalla: 'lista',
            productos: this.state.productos
        });
    }


    editProducto(id) {
        this.getItem('categorias').then(
            data => {
                let categoriasTemp = []
                data.map(
                    item => {
                        const categoria = {
                            label: item.nombre,
                            value: item.nombre
                        }
                        categoriasTemp.push(categoria);
                        this.setState({
                            categorias: categoriasTemp
                        });
                    }
                );
                this.state.categorias = data;
                console.log(data);
                console.log(id);
                let editar = {};
                this.state.productos.map(data => {
                    if (data.id == id) {
                        editar = data;
                    }
                })
                console.log(editar);
                this.setState({
                    producto: editar,
                    pantalla: "editar"
                })
            }
        )


    }

    confirmarEditarProducto() {
        console.log('confirmar edicion');
        console.log(this.state.producto);
        let index = -1;
        this.state.productos.map(
            item => {
                if (item.id == this.state.producto.id) {
                    index = this.state.productos.indexOf(item);
                }
            }
        )
        this.state.productos[index] = this.state.producto;
        AsyncStorage.setItem('productos', JSON.stringify(this.state.productos));
        this.setState({
            productos: this.state.productos,
            pantalla: "lista"
        });
    }

    removeProducto(id) {
        console.log(id);
        let eliminar = {};
        this.state.productos.map(data => {
            if (data.id == id) {
                eliminar = data;
            }
        })
        const index = this.state.productos.indexOf(eliminar);
        this.state.productos.splice(index, 1);
        this.setState({ productos: this.state.productos });
        AsyncStorage.setItem('productos', JSON.stringify(this.state.productos));

    }

    getMaxIdPlus() {
        let id = 0;
        this.state.productos.map(data => {
            if (data.id > id) {
                id = data.id
            }
        })
        return id + 1;
    }


    getRowData(lista) {
        const tableData = [];
        lista.map(data => {
            const rowData = [];
            rowData.push(`${data.id}`);
            rowData.push(`${data.nombre}`);
            rowData.push(`${data.codigo}`);
            rowData.push(`${data.categoria}`);
            rowData.push(`${data.precio}`);
            rowData.push('editar');
            tableData.push(rowData);
        })
        console.log(tableData);
        return tableData;
    }

    render() {
        let state = this.state;
        this.getItem("recargarProductos").then(
            recargar => {
                console.log(recargar);
                if (recargar) {
                    this.getItem("productos").then(
                        data => {
                            console.log(data);
                            if (data) {
                                console.log(data);
                                this.setState({
                                    productos: data
                                });
                            } else {
                                const productosTemp = [{
                                    id: 1,
                                    nombre: 'Gaseosa',
                                    codigo: '1231',
                                    categoria: 'Bebida',
                                    precio: 1000
                                }]
                                AsyncStorage.setItem('productos', JSON.stringify(productosTemp));

                            }

                        }
                    );
                    AsyncStorage.setItem('recargarProductos', "false");
                }

            }

        );

        const tableDataProductos = this.getRowData(this.state.productos);
        const element = (id, data, index) => (
            <View>
                <TouchableOpacity onPress={() => this.editProducto(id)}>
                    <View style={stylesTable.btn}>
                        <Text style={stylesTable.btnText}>Editar</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.removeProducto(id)}>
                    <View style={{ width: 58, height: 18, backgroundColor: 'red', borderRadius: 2 }}>
                        <Text style={stylesTable.btnText}>Eliminar</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
        if (state.pantalla == 'lista') {
            return (
                <View>
                    <View style={{ flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' }}>
                        <ScrollView horizontal={true}>
                            <Table borderStyle={{ borderWidth: 1, borderColor: '#C1C0B9' }}>
                                <Row data={state.tableHead} style={stylesTable.head} textStyle={stylesTable.text} />
                                {
                                    tableDataProductos.map((rowData, index) => (
                                        <TableWrapper key={index} style={stylesTable.row}>
                                            {
                                                rowData.map((cellData, cellIndex) => (
                                                    <Cell key={cellIndex} data={cellIndex === 5 ? element(rowData[0], cellData, index) : cellData} textStyle={stylesTable.text} />
                                                ))
                                            }
                                        </TableWrapper>
                                    ))
                                }
                            </Table>

                        </ScrollView>
                        <AppButton
                            bgColor="rgba(14, 213, 100, 0.57)"
                            title="Agrega Producto"
                            action={this.addProducto.bind(this)}
                            iconName="plus"
                            iconSize={30}
                            iconColor="#fff"
                        />
                    </View>
                </View>
            );
        } else if (state.pantalla == 'nuevo') {
            let producto = this.state.producto;
            let value = null;
            return (
                <ScrollView>
                    <TextInput
                        style={styles.input}
                        placeholder='ID'
                        editable={false}
                        value={producto.id.toString()}
                        onChangeText={(value) => {
                            producto.id = value;
                            this.setState({ producto: producto })
                            // console.log(producto);
                        }}
                    />

                    <TextInput
                        style={styles.input}
                        placeholder='Nombre'
                        value={producto.nombre}
                        onChangeText={(value) => {
                            producto.nombre = value;
                            this.setState({ producto: producto })
                        }}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder='Codigo'
                        value={producto.codigo}
                        onChangeText={(value) => {
                            producto.codigo = value.replace(/[^0-9]/g, '');
                            this.setState({ producto: producto })
                        }}
                    />
                    <Dropdown
                        style={styles.dropdown}
                        placeholderStyle={styles.placeholderStyle}
                        selectedTextStyle={styles.selectedTextStyle}
                        inputSearchStyle={styles.inputSearchStyle}
                        iconStyle={styles.iconStyle}
                        data={this.state.categorias}
                        search
                        maxHeight={300}
                        labelField="label"
                        valueField="value"
                        placeholder="Categoria"
                        value={value}
                        onChange={item => {
                            producto.categoria = item.value;
                            this.setState({ producto })
                        }}
                        renderLeftIcon={() => (
                            <AntDesign style={styles.icon} color="black" name="Safety" size={20} />
                        )}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder='Precio'
                        value={producto.precio}
                        onChangeText={(value) => {
                            producto.precio = value.replace(/[^0-9]/g, '');
                            this.setState({ producto: producto })
                        }}
                    />
                    <AppButton
                        bgColor="rgba(14, 213, 100, 0.57)"
                        title="Guardar Producto"
                        action={this.guardarProducto.bind(this)}
                        iconName="plus"
                        iconSize={30}
                        iconColor="#fff"
                    />
                </ScrollView>
            );

        } else {
            let producto = this.state.producto;
            let value = null;
            return (
                <ScrollView>
                    <TextInput
                        style={styles.input}
                        placeholder='ID'
                        editable={false}
                        value={producto.id.toString()}
                        onChangeText={(value) => {
                            producto.id = value;
                            this.setState({ producto: producto })
                            // console.log(producto);
                        }}
                    />

                    <TextInput
                        style={styles.input}
                        placeholder='Nombre'
                        value={producto.nombre}
                        onChangeText={(value) => {
                            producto.nombre = value;
                            this.setState({ producto: producto })
                        }}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder='Codigo'
                        value={producto.codigo}
                        onChangeText={(value) => {
                            producto.codigo = value.replace(/[^0-9]/g, '');
                            this.setState({ producto: producto })
                        }}
                    />
                    <Dropdown
                        style={styles.dropdown}
                        placeholderStyle={styles.placeholderStyle}
                        selectedTextStyle={styles.selectedTextStyle}
                        inputSearchStyle={styles.inputSearchStyle}
                        iconStyle={styles.iconStyle}
                        data={this.state.categorias}
                        search
                        maxHeight={300}
                        labelField="label"
                        valueField="value"
                        placeholder="Categoria"
                        value={value}
                        onChange={item => {
                            producto.categoria = item.value;
                            this.setState({ producto })
                        }}
                        renderLeftIcon={() => (
                            <AntDesign style={styles.icon} color="black" name="Safety" size={20} />
                        )}
                    />
                    <TextInput
                        style={styles.input}
                        placeholderStyle={styles.placeholderStyle}
                        placeholder='Precio'
                        value={producto.precio}
                        onChangeText={(value) => {
                            producto.precio = value.replace(/[^0-9]/g, '');
                            this.setState({ producto: producto })
                        }}
                    />
                    <AppButton
                        bgColor="rgba(14, 213, 100, 0.57)"
                        title="Editar Producto"
                        action={this.confirmarEditarProducto.bind(this)}
                        iconName="plus"
                        iconSize={30}
                        iconColor="#fff"
                    />
                </ScrollView>
            );

        }

    }
}