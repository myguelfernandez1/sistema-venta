import React, { Component } from "react";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Text, View, ScrollView, TouchableOpacity, StyleSheet, StatusBar, TextInput } from 'react-native';
import { Table, TableWrapper, Row, Rows, Col, Cell } from 'react-native-table-component'
import AppButton from "./AppButton";


const stylesTable = StyleSheet.create({
    container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
    head: { height: 40, backgroundColor: '#808B97' },
    text: { margin: 6 },
    row: { flexDirection: 'row', backgroundColor: '#E7E6E1' },
    btn: { width: 58, height: 18, backgroundColor: '#78B7BB', borderRadius: 2 },
    btnText: { textAlign: 'center', color: '#fff' }
});
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        backgroundColor: '#E0E0E0',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
    },
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
    }
});

export default class Categorias extends Component {


    constructor() {
        super();
        this.state = {
            categorias: [],
            tableHead: ['Id', 'Nombre', ''],
            pantalla: 'lista',
            categoria: {}
        }
        AsyncStorage.setItem('recargarCategorias', "true");

    }


    async getItem(item) {
        const result = await AsyncStorage.getItem(item);
        return JSON.parse(result);
    }

    async addCategoria() {
        console.log('add categoria');
        this.setState({
            pantalla: 'nuevo',
            categoria: {
                id: this.getMaxIdPlus(),
                nombre: ''
            }
        });
    }

    async guardarCategoria() {
        console.log('guardar categoria');
        console.log(this.state.categoria);
        this.state.categorias.push(this.state.categoria);
        console.log(this.state);
        AsyncStorage.setItem('categorias', JSON.stringify(this.state.categorias));
        this.setState({
            pantalla: 'lista',
            categorias: this.state.categorias
        });
    }


    editCategoria(id) {
        console.log(id);
        let editar = {};
        this.state.categorias.map(data => {
            if (data.id == id) {
                editar = data;
            }
        })
        console.log(editar);
        this.setState({
            categoria: editar,
            pantalla: "editar"
        })

    }

    confirmarEditarCategoria() {
        console.log('confirmar edicion');
        console.log(this.state.categoria);
        let index = -1;
        this.state.categorias.map(
            item => {
                if (item.id == this.state.categoria.id) {
                    index = this.state.categorias.indexOf(item);
                }
            }
        )
        this.state.categorias[index] = this.state.categoria;
        AsyncStorage.setItem('categorias', JSON.stringify(this.state.categorias));
        this.setState({
            categorias: this.state.categorias,
            pantalla: "lista"
        });
    }

    removeCategoria(id) {
        console.log(id);
        let eliminar = {};
        this.state.categorias.map(data => {
            if (data.id == id) {
                eliminar = data;
            }
        })
        const index = this.state.categorias.indexOf(eliminar);
        this.state.categorias.splice(index, 1);
        this.setState({ categorias: this.state.categorias });
        AsyncStorage.setItem('categorias', JSON.stringify(this.state.categorias));

    }

    getMaxIdPlus() {
        let id = 0;
        this.state.categorias.map(data => {
            if (data.id > id) {
                id = data.id
            }
        })
        return id + 1;
    }


    getRowData(lista) {
        const tableData = [];
        lista.map(data => {
            const rowData = [];
            rowData.push(`${data.id}`);
            rowData.push(`${data.nombre}`);
            rowData.push('editar');
            tableData.push(rowData);
        })
        return tableData;
    }

    render() {
        let state = this.state;
        this.getItem("recargarCategorias").then(
            recargar => {
                console.log(recargar);
                if (recargar) {
                    this.getItem("categorias").then(
                        data => {
                            console.log(data);
                            if (data) {
                                console.log(data);
                                this.setState({
                                    categorias: data
                                });
                            } else {
                                const categoriasTemp = [{
                                    id: 1,
                                    nombre: 'bebidas'
                                }]
                                AsyncStorage.setItem('categorias', JSON.stringify(categoriasTemp));

                            }

                        }
                    );
                    AsyncStorage.setItem('recargarCategorias', "false");
                }

            }

        );

        const tableDataCategorias = this.getRowData(this.state.categorias);
        const element = (id, data, index) => (
            <View>
                <TouchableOpacity onPress={() => this.editCategoria(id)}>
                    <View style={stylesTable.btn}>
                        <Text style={stylesTable.btnText}>Editar</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.removeCategoria(id)}>
                    <View style={{ width: 58, height: 18, backgroundColor: 'red', borderRadius: 2 }}>
                        <Text style={stylesTable.btnText}>Eliminar</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
        if (state.pantalla == 'lista') {
            return (
                <ScrollView>
                    <Table borderStyle={{ borderColor: 'transparent' }}>
                        <Row data={state.tableHead} style={stylesTable.head} textStyle={stylesTable.text} />
                        {
                            tableDataCategorias.map((rowData, index) => (
                                <TableWrapper key={index} style={stylesTable.row}>
                                    {
                                        rowData.map((cellData, cellIndex) => (
                                            <Cell key={cellIndex} data={cellIndex === 2 ? element(rowData[0], cellData, index) : cellData} textStyle={stylesTable.text} />
                                        ))
                                    }
                                </TableWrapper>
                            ))
                        }
                    </Table>
                    <AppButton
                        bgColor="rgba(14, 213, 100, 0.57)"
                        title="Agrega Categoria"
                        action={this.addCategoria.bind(this)}
                        iconName="plus"
                        iconSize={30}
                        iconColor="#fff"
                    />
                </ScrollView>
            );
        } else if (state.pantalla == 'nuevo') {
            let categoria = this.state.categoria;
            return (
                <ScrollView>
                    <TextInput
                        style={styles.input}
                        placeholder='ID'
                        editable={false}
                        value={categoria.id.toString()}
                        onChangeText={(value) => {
                            categoria.id = value;
                            this.setState({ categoria: categoria })
                            // console.log(categoria);
                        }}
                    />

                    <TextInput
                        style={styles.input}
                        placeholder='Nueva Categoria'
                        value={categoria.nombre}
                        onChangeText={(value) => {
                            categoria.nombre = value;
                            this.setState({ categoria: categoria })
                        }}
                    />

                    <AppButton
                        bgColor="rgba(14, 213, 100, 0.57)"
                        title="Guardar Categoria"
                        action={this.guardarCategoria.bind(this)}
                        iconName="plus"
                        iconSize={30}
                        iconColor="#fff"
                    />
                </ScrollView>
            );

        } else {
            let categoria = this.state.categoria;
            return (
                <ScrollView>
                    <TextInput
                        style={styles.input}
                        placeholder='ID'
                        editable={false}
                        value={categoria.id.toString()}
                        onChangeText={(value) => {
                            categoria.id = value;
                            this.setState({ categoria: categoria })
                            // console.log(categoria);
                        }}
                    />

                    <TextInput
                        style={styles.input}
                        placeholder='Nueva Categoria'
                        value={categoria.nombre}
                        onChangeText={(value) => {
                            categoria.nombre = value;
                            this.setState({ categoria: categoria })
                        }}
                    />

                    <AppButton
                        bgColor="rgba(14, 213, 100, 0.57)"
                        title="Editar Categoria"
                        action={this.confirmarEditarCategoria.bind(this)}
                        iconName="plus"
                        iconSize={30}
                        iconColor="#fff"
                    />
                </ScrollView>
            );

        }

    }
}