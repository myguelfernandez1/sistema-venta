import React, { Component } from "react";
import { Button, ScrollView, Text } from "react-native";

import { printToFileAsync } from 'expo-print';
import { shareAsync } from 'expo-sharing';

const html = `
    <html>
      <body>
        <h1> Hola ${name}</h1>
        <p style="color: red; ">Hola como estas </p>
      </body>
    </html>
  `;

export default class Venta extends Component {
    constructor() {
        super();
    }


    generatePdf = async () => {
        const file = await printToFileAsync({
            html: html,
            base64: false
        });

        await shareAsync(file.uri);
    };

    render() {
        return (
            <ScrollView>
                <Text>
                    Hola Venta
                </Text>
                <Button title='Gerarar PDF' onPress={this.generatePdf}></Button>
            </ScrollView>
        );
    }
}