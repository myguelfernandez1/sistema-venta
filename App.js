import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';
import { useState } from 'react';
import { printToFileAsync } from 'expo-print';
import { shareAsync } from 'expo-sharing';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Categorias from './src/components/Categorias';
import Venta from './src/components/Venta';
import Clientes from './src/components/Cliente';
import Productos from './src/components/Productos';
import Ventas from './src/components/Ventas';

import AsyncStorage from '@react-native-async-storage/async-storage';


function HomeScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button
        onPress={() => console.log('bienvenido')}
        title="Bienvenido"
      />
    </View>
  );
}



function CategoriasScreen({ route, navigation }) {
  console.log("cuando entra en categoria Screen");
  console.log(route.params);
  return (
    <Categorias
      navegar={(pantalla, categoria) => {
        console.log(pantalla);
        navigation.navigate(pantalla, categoria)
      }}
    />
  );
}

function ClientesScreen({ route, navigation }) {
  console.log("cuando entra en cliente Screen");
  console.log(route.params);
  return (
    <Clientes
      navegar={(pantalla, cliente) => {
        console.log(pantalla);
        navigation.navigate(pantalla, cliente)
      }}
    />
  );
}

function ProductosScreen({ route, navigation }) {
  console.log("cuando entra en producto Screen");
  console.log(route.params);
  return (
    <Productos
      navegar={(pantalla, producto) => {
        console.log(pantalla);
        navigation.navigate(pantalla, producto)
      }}
    />
  );
}

function VentasScreen({ route, navigation }) {
  console.log("cuando entra en venta Screen");
  console.log(route.params);
  
  return (
    <Ventas
      navegar={(pantalla, venta) => {
        console.log(pantalla);
        navigation.navigate(pantalla, venta)
      }}
    />
  );
}


function VentaScreen({ route, navigation }) {
  console.log("cuando entra en categoria Screen");
  console.log(route.params);
  AsyncStorage.setItem('recargarVentas', "true");
  return (
    <Venta
      navegar={(pantalla, categoria) => {
        console.log(pantalla);
        navigation.navigate(pantalla, categoria)
      }}
    />
  );
}

const Drawer = createDrawerNavigator();
const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: 'rgb(255, 45, 85)',
  },
};

const MyTheme2 = {
  dark: true,
  colors: {
    primary: 'rgb(255, 45, 85)',
    background: 'rgb(242, 242, 242)',
    card: 'rgb(255, 255, 255)',
    text: 'rgb(28, 28, 30)',
    border: 'rgb(199, 199, 204)',
    notification: 'rgb(255, 69, 58)',
  },
};


export default function App() {

  // let [name, setName] = useState("");



  let generatePdf = async () => {
    const file = await printToFileAsync({
      html: html,
      base64: false
    });

    await shareAsync(file.uri);
  };

  return (
    // <View style={styles.container}>
    //   <TextInput value={name}
    //     placeholder='texto a imprimir'
    //     style={styles.textInput} onChangeText={(value) => setName(value)}
    //   />
    //   <Button title='Gerarar PDF'  onPress={generatePdf}></Button>
    //   <StatusBar style='auto'/>
    // </View>

    <NavigationContainer theme={MyTheme2}>
      <Drawer.Navigator initialRouteName="Ventas">
        <Drawer.Screen name="Home" component={HomeScreen} />
        <Drawer.Screen name="Categorias" component={CategoriasScreen} />
        {/* <Drawer.Screen name="Venta" component={VentaScreen} /> */}
        <Drawer.Screen name="Clientes" component={ClientesScreen} />
        <Drawer.Screen name="Productos" component={ProductosScreen} />
        <Drawer.Screen name="Ventas" component={VentasScreen} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textInput: {
    alignSelf: "stretch",
    padding: 8,
    margin: 8
  }
});
